(function(){
"use strict";





// Angular Code
// ============
var app = angular.module('app', ['angular-progress-arc']);

app.controller('Dashboard', ['$scope', '$http', '$timeout', function($scope, $http, $timeout){
	
	// Prefixes
	$scope.imgUrl = 'img/';
	$scope.userUrl = '#!/user/'

	// @param: Int minutes
	// @desc: returns current date or date - minutes
	$scope.getDatetime = function(mins){
		if(mins && +mins){
			var date = new Date;
			var buildDate = new Date(date - mins * 60000);
			return buildDate;
		}
		else{
			return new Date;
		}
	}

	// @desc: returns status for metric panel
	$scope.getMetricStatus = function(val1, val2){
	  if(val1 === val2){
	  	return 'stable';
	  }
	  else if(val1 < val2){
	  	return 'down';
	  }
	};

	// @desc: returns terminal text color
	$scope.getTerminalStatus	= function(val1, val2){
	  if(val2){
	  	val1 = val1 / (val1 + val2) * 100;
	  }
	  if(val1 < 10){
	  	return 'Text-red';
	  }
	  else if(val1 < 50){
	  	return 'Text-warn';
	  }
	  else{
	  	return 'Text-green';
	  }
	}

	// Mock API call
	// @note: This is just getting a json file, in production would be a call to a rest api.
	// You could poll this with $timeout 
	// OR if you want to setup sockets: 
	// http://stackoverflow.com/questions/11276520/can-angular-js-auto-update-a-view-if-a-persistent-model-server-database-is-cha#answer-11277751
	$scope.loadItems = function(){
		$http.get('api/items.json').success(function(data){
			$scope.items = data;
		})
		.error(function(){
		 	if(window.console){
		   console.log('error loading items')
		 	}
		});
	};
	$scope.loadItems();

	

	// @desc: controls the open/close state of the items
	$scope.setOpenItem = function(item){
		//@todo: address this duplication of code
		if(item.open){
			$scope.items.forEach(function(item, i){
				item.open = false;
			});
		}
		else{
			$scope.items.forEach(function(item, i){
				item.open = false;
			});
			item.open = true;
		}
	}

	// @desc: returns either mins / hours / days value
	$scope.timeValue = function(val){
		if(val < 60){
			return val;
		}
		else if(val < 1440){
			return Math.floor(val / 60);
		}
		else{
			return Math.floor(val / 1440);
		}
	};

	//@desc: returns either mins/hours/days based on the time value
	$scope.timeMetric = function(val){
	  if(val < 60){
	  	if(val < 2){
	  		return 'min';
	  	}
	  	else{
	  		return 'mins';
	  	}
	  }
	  else if(val < 1440){
	  	if(val < 120){
	  		return 'hour';
	  	}
	  	else{
	  		return 'hours';
	  	}
	  }
	  else{
	  	if(val < 2880){
	  		return 'day';
	  	}
	  	else{
	  		return 'days';
	  	}
	  }
	};

	// @desc: Inits tooltips, Fires on the last ngInclude load.
	// Uses items count and ngIncludeCount to ensure it only fires on the last onload.
	var count, ngIncludeIndex = 0;
	$scope.tooltipInit = function(){
		count || (count = $scope.items.length);
		if(++ngIncludeIndex === count && $("[rel=tooltip]").length){
			$timeout(function(){
			  $("[rel=tooltip]").tooltip({container: 'body'});
			});
		}
	}

}]);



// @desc: capitalizes text
app.filter('capitalize', function() {
  return function(input, all) {
    return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
  }
});


})();
