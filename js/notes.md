React notes

1) createClass Render Method
- Classes must contain a render method, it looks like:
render: function(){
  return (
    <div className="parentNode">
     <h1>sup homey</h1>
    </div>
  )
}

- render can only return one root node.

- You must use an uppercase variable to define your class/custom element.



2) Custom properties
- You can add custom properties to the node as it's being rendered.
eg. React.render(<App title="test" />, document.body);

- You have access to these props with this.props.title
- To access inside jsx use {this.props.title}

Properties are when we want to pass in static information, that we know is not going to change, I assume properties are faster.


3) propTypes [Object]
=====================
You can Define the properties you want to expect using the propTypes method.

propTypes: {
  txt: React.PropTypes.string,
  cat: React.PropTypes.number
}

*By itself these proptypes are are optional, and are there for dev knowledge, if you add .isRequired to the type it will throw a console warning when a prop is missing.

propTypes: {
  txt: React.PropTypes.string.isRequired,
  cat: React.PropTypes.number.isRequired
}


4) getDefaultProps [Function] -> [Object]
=============================
return default properties



5) State
@desc: Properties that are going to change

5.1) getInitialState [Function] -> [Object]
===============================

To access use this.state.title

5.2) Inside any function
this.setState({
  val: 1
})



6) Owner / Ownee
================
When a component creates another component inside itself, this is referred to as the owner / ownee relationship


7) Custom functions
======
You can easily pass custom methods to the component, do not pass inside a string.

<div update={this.myCustomMethod} ></div>

Then access them from within jsx

<input onChange={this.props.update} />


8) Refs
=======
Provides a reference to a dom node/component,
<input ref="myRef" />
to access a dom node, you can use this.refs.myRef.getDOMNode()


9) this.props.children
=================
Allows for nested components

<Elem>
  <h1>child</h1>
</Elem>

access in render using:
{this.props.children}


10) Spread operator
========================
Pass properties from your outer component to the inner dom component.
<h1 {...this.props}>Hello World!</h1>;

Any property you specify on the component will get passed onto the dom node.


/*** egg5.html ***/

11) Component Lifecycle: Mounting
=================================

11.1) componentWillMount [Function]()
Fires before the component renders for the first time

11.2) componentDidMount [Function]()
Fires after the component renders for the first time, you can access the DOM node with this.getDOMNode()

11.2) componentWillUnmount [Function]()
Fires when the component is about to unmount

11.3) React.unmountComponentAtNode(document.getElementById('myNode'))
Unmounts the node


/*** egg6.html ***/

12) Component Lifecycle: Updating
=================================

12.1) componentWillReceiveProps [Function](nextProps)
Makes props changes pre render

12.2) shouldComponentUpdate [Function](nextProps, nextState)
Great for optimizing dom interaction.
If returns false, won't trigger the render method, state will still update but the view won't.

12.3) componentWillUpdate [Function](nextProps, nextState)
Fires prior to the component re-rendering, has access to the props/state that's about to render.

12.4) componentDidUpdate [Function](prevProps, prevState)
Fires after the component re-renders.


/*** egg7.html ***/

13) Mixins
==========
Inherit functionality, and reuse functionality across components.

Specifically for React Built in methods & any custom methods that need access to React's methods/props.

The inherited mixin gets called first, then if the same method is present in the component, that gets called.

var myMixin = {
  componentWillMount: function(){
    clog('hi there')
  }
}

var CustomElem = React.createClass({
  mixins: [myMixin],
  componentWillMount: function(){
    clog('hey ho')
  }
});

> hi there
> hey ho



14) Dynamically Generating Components
=====================================
var dataset = [
  {id: 1, name: 'gk', age: 24}
];

// Outside component: I deal with the data / provide the api
var PersonTable = React.createClass({
  getInitialState: function(){
    return {
      data: dataset
    }
  },
  render: function(){
    return (
      this.state.data.map(function(item, i){
        return (
          <Person id={item.id} name={item.name} age={item.age} />
        )
      });
    )
  }
});

// Inner Component: I am the actual object, I only rely on props from the parent
var Person = React.createClass({
  render: function(){
    return(
      <h1>My name is {this.props.name}</h1>
      <p>My age is {this.props.age}</p>
    )
  }
})


15) show/hide (as well as proper parent -> child data handling)
=============
var Form = React.createClass({
  getInitialState: function() {
    return {
      err: ''
    };
  },
  update: function(e){
    if(e.target.value < 5){
      this.setState({
        err: 'text too short'
      })
    }
    else{
      this.setState({
        err: ''
      });
    }
  },
  render: function(){
    return (
      <div>
        <input onChange={this.update} />
        <ErrorBar error={this.state.err} />
      </div>
    )
  }
});

var ErrorBar = React.createClass({
  render: function() {
    if(this.props.error){
      return (
        <div className="row">
          <p className="alert alert-danger">{this.props.error}</p>
        </div>
      );  
    }
    else{
      return null;
    } 
  }
});


16) JSX Deep Dive
=================
Custom Properties can still be referenced in react, but will not be rendered to the browser.

eg) <div my-name="Grant"></div>
will not show up in the dom

*If you want it to show up you have to use <div data-my-name="Grant"></div>


17) React Link (two way binding helper)
==============
-- Old way using getDOMNode() --

var UserForm = React.createClass({
  mixins: [React.addons.LinkedStateMixin],
  getInitialState: function() {
    return {
      name: '',
      email: ''
    };
  },
  update: function(){
    this.setState({
      name: this.refs.name.getDOMNode().value,
      email: this.refs.email.getDOMNode().value
    });
  },
  render: function() {
    return (
      <form>
        <div>
          <input ref="name" onChange={this.update} type="text" placeholder="Name" />
          <label>*{this.state.name}*</label>
        </div>
        <div>
          <input ref="email" onChange={this.update} type="text" placeholder="Email" />
          <label>*{this.state.email}*</label>
        </div>
      </form>
    );
  }
});


-- New way --
mixins: [React.addons.LinkedStateMixin]

<input valueLink={this.linkState('name')} type="text" placeholder="Name" />
<label>*{this.state.name}*</label>




18) classSet
============
Set classes based on a condition

render: function(){
  var classes = React.addons.classSet({
    'myClass': myVal > 2
  });
  return <div className={classes}></div>
}


19) Simulate events (testing)
=============================

var test = React.addons.TestUtils;
var app = React.render(<App />, document.body);
test.Simulate.click(app.refs.myNode.getDOMNode());








