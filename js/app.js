"use strict";

window.UTIL = {};


//UTIL methods
(function () {
  //Detect mobile devices
  //updated: jun 22 2014
  //Check if new regex is being used
  //go here -> http://detectmobilebrowsers.com/
  //then here -> http://jsbeautifier.org/
  //copy what's between the if statement
  UTIL.isMobile = (function () {
    var a = navigator.userAgent || navigator.vendor || window.opera;
    //also check's ipad
    if (/(iPhone|iPod|iPad).*AppleWebKit/i.test(navigator.userAgent)) return true;
    return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4));
  })();


  UTIL.mobileFlag = /\?mobile/.test(window.location.href);

  var timer = {};
  var firstCall = {};

  UTIL.timer = function timer(func, time, amount) {
    clearTimeout(timer);
    var name, reg;

    // Find the function name
    if (Function.name) {
      name = func.name;
    } else {
      reg = func.toString().match(/^function\s*([^\s(]+)/);
      if (reg && reg[1]) {
        name = reg[1];
      }
    }

    // Set the timeout
    if (name) {
      timer[name] = setTimeout(func, time || 0);
    } else {
      name = "name";
      setTimeout(func, time || 0);
    }

    if (amount > 0) {
      if (!firstCall[name]) {
        firstCall[name] = true;
        --amount;
      }

      setTimeout(function () {
        UTIL.timer(func, time || 0, --amount);
      }, time || 0);
    }
  };
})();
"use strict";

(function () {
  "use strict";





  // Angular Code
  // ============
  var app = angular.module("app", ["angular-progress-arc"]);

  app.controller("Dashboard", ["$scope", "$http", "$timeout", function ($scope, $http, $timeout) {
    // Prefixes
    $scope.imgUrl = "img/";
    $scope.userUrl = "#!/user/";

    // @param: Int minutes
    // @desc: returns current date or date - minutes
    $scope.getDatetime = function (mins) {
      if (mins && +mins) {
        var date = new Date();
        var buildDate = new Date(date - mins * 60000);
        return buildDate;
      } else {
        return new Date();
      }
    };

    // @desc: returns status for metric panel
    $scope.getMetricStatus = function (val1, val2) {
      if (val1 === val2) {
        return "stable";
      } else if (val1 < val2) {
        return "down";
      }
    };

    // @desc: returns terminal text color
    $scope.getTerminalStatus = function (val1, val2) {
      if (val2) {
        val1 = val1 / (val1 + val2) * 100;
      }
      if (val1 < 10) {
        return "Text-red";
      } else if (val1 < 50) {
        return "Text-warn";
      } else {
        return "Text-green";
      }
    };

    // Mock API call
    // @note: This is just getting a json file, in production would be a call to a rest api.
    // You could poll this with $timeout
    // OR if you want to setup sockets:
    // http://stackoverflow.com/questions/11276520/can-angular-js-auto-update-a-view-if-a-persistent-model-server-database-is-cha#answer-11277751
    $scope.loadItems = function () {
      $http.get("api/items.json").success(function (data) {
        $scope.items = data;
      }).error(function () {
        if (window.console) {
          console.log("error loading items");
        }
      });
    };
    $scope.loadItems();



    // @desc: controls the open/close state of the items
    $scope.setOpenItem = function (item) {
      //@todo: address this duplication of code
      if (item.open) {
        $scope.items.forEach(function (item, i) {
          item.open = false;
        });
      } else {
        $scope.items.forEach(function (item, i) {
          item.open = false;
        });
        item.open = true;
      }
    };

    // @desc: returns either mins / hours / days value
    $scope.timeValue = function (val) {
      if (val < 60) {
        return val;
      } else if (val < 1440) {
        return Math.floor(val / 60);
      } else {
        return Math.floor(val / 1440);
      }
    };

    //@desc: returns either mins/hours/days based on the time value
    $scope.timeMetric = function (val) {
      if (val < 60) {
        if (val < 2) {
          return "min";
        } else {
          return "mins";
        }
      } else if (val < 1440) {
        if (val < 120) {
          return "hour";
        } else {
          return "hours";
        }
      } else {
        if (val < 2880) {
          return "day";
        } else {
          return "days";
        }
      }
    };

    // @desc: Inits tooltips, Fires on the last ngInclude load.
    // Uses items count and ngIncludeCount to ensure it only fires on the last onload.
    var count, ngIncludeIndex = 0;
    $scope.tooltipInit = function () {
      count || (count = $scope.items.length);
      if (++ngIncludeIndex === count && $("[rel=tooltip]").length) {
        $timeout(function () {
          $("[rel=tooltip]").tooltip({ container: "body" });
        });
      }
    };
  }]);



  // @desc: capitalizes text
  app.filter("capitalize", function () {
    return function (input, all) {
      return !!input ? input.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }) : "";
    };
  });

})();
"use strict";

// =================================================
// Name: Create JS
// Details: a small library that extracts the menial tasks out of OO programming in JavaScript
// and also adds some extra functionality that you would have to hand-roll with new/Object.create/es6 classes.
// Version: 0.9
// Author: Grant Kiely
// =====================

// Todo...
// Create inheritance for all createClass methods, if you create a method that also has a parent method of the same name
// it will get converted to an array and both will get called by default.
// clean up, make functions modular & nice, comment, maybe start from scratch and do a rewrite

var create = (function () {
  // ================
  // Internal Methods
  // ================
  // ===========================================================
  // Copies properties of passed in object to child object
  // @params:  child -> child object, obj -> object passed to create()
  // @return: no return needed as Object.create is pbr
  // ===========================================================
  function addProps(child, obj) {
    for (var i in obj) {
      child[i] = obj[i];
    }
  }

  // ===================================
  // Overwrites and adds required params
  // @params: child -> child object, req -> required params in object, obj -> object passed to create()
  // @return: no return
  // ==================
  function addReqs(child, req, obj) {
    if (obj.req) {
      child.req = obj.req;
    } else if (req) {
      child.req = obj.__req || {};
      for (var i in req) {
        child.req[i] = req[i];
      }
    }
  }

  // ====================================================================
  // Checks requirement object, and throws error if property not present.
  // @params: obj -> object passed to create() these are the props we are checking,
  // req -> req object to test against, parent -> parent object
  // @return: no return
  // ==================
  function checkReq(obj, req, parent) {
    var err = [],
        typeStr,
        name = parent._name;

    for (var i in req) {
      if (obj && obj.hasOwnProperty(i)) {
        typeVal = typeof req[i];
        if (typeVal === "function") {
          if (!req[i](obj[i])) {
            throw new Error("parameter: [" + i + "] did not meed param requirements");
          }
        } else if (typeVal === "object" && Object.keys(req[i]).length) {
          //This currently only allows 1 test & msg, can update for more in future
          if (req[i].test && !req[i].test(obj[i])) {
            throw new Error("parameter: [" + i + "] " + req[i].msg(obj[i]) || "did not meed param requirements");
          }
        }
      } else if (!parent.hasOwnProperty(i)) {
        err.push(i);
      }
    }
    if (err.length) {
      throw new Error(name + " instance missing parameter" + (err.length > 1 ? "s" : "") + ": " + err.join(", "));
    }
  }

  // ===========================================================================================
  // Get's all properties that aren't functions, have the name of req, or start with _ (private)
  // Useful for testing req's, note: you can't req functions
  // @params: parent -> parent object
  // @return: object with filtered properties
  // ========================================
  function getFilteredProps(parent) {
    var obj = {};
    for (var i in parent) {
      if (typeof parent[i] !== "function" && i !== "req" && i.charAt(0) !== "_") {
        obj[i] = parent[i];
      }
    }
    return obj;
  }



  // ==========
  // Public API
  // ==========
  // =============================
  // Public wrapper for createClass
  // Checks for function, if name, adds reference to it, calls create without init.
  // @param: parent -> parent object (optional for inheritance)
  // @param: func -> can pass function or {}, added benefit to function is the name prop, you can query inherited parent.
  // @return: new class object that other objects can inherit from
  // =============================================================
  window.createClass = function createClass(parent, func) {
    var name = parent.name, obj;
    if (typeof parent === "function") {
      parent = parent();
      if (name) {
        parent._name = name;
      }
      return parent;
    }
    if (typeof func === "function") {
      name = func.name;
      obj = func() || {};
      obj._name = name;
      obj._parent = parent._name;
    } else if (typeof func === "object") {} else {
      return parent;
    }
    obj = obj || func;
    return create(parent, obj, false);
  };


  // ====================
  // Main create function
  // Creates new object from existing
  // Utilises Object.create, auto calls init(), check's required params
  // @params: parent -> parent object, obj -> passed in object, init -> bool flag to call init/check req's
  // @return: new object
  // ===================
  return function create(parent, obj, init) {
    var child = Object.create(parent),
        req = obj.req || parent.req;

    if (obj) {
      addProps(child, obj);
      addReqs(child, req, obj);
    }
    if (init !== false) {
      // If object do normal checkReq
      if (typeof child.req === "object") checkReq(obj, child.req, parent);
      // If string get do full check, does not include vars prefixed with _
      else if (typeof child.req === "string" && child.req.toLowerCase() === "all") {
        var tempObj = getFilteredProps(parent);
        checkReq(obj, tempObj, parent);
      }
    }


    //Inheritance of init
    if (typeof child.init === "function" && init !== false) {
      child.init();
    } else if (typeof child.init === "object" && parent.init.length !== undefined && init !== false) {
      child.init.forEach(function (f) {
        f();
      });
    } else if (typeof child.init === "object" && child.init.length !== undefined) {} else if (typeof parent.init === "object" && parent.init.length !== undefined) {
      var temp = [];
      temp = parent.init.slice(0);
      temp.push(child.init);
      child.init = temp;
    } else if (typeof parent.init === "function" && child.init) {
      if (typeof child.init === "function") {
        child.init = [parent.init, child.init];
      }
    }

    return child;
  };
})();


// ======================
// Object.create Polyfill
// ======================
if (typeof Object.create != "function") {
  (function () {
    var F = function () {};Object.create = function (o) {
      if (arguments.length > 1) {
        throw Error("Second argument not supported");
      }if (typeof o != "object") {
        throw TypeError("Argument must be an object");
      }F.prototype = o;return new F();
    };
  })();
}

// ======================
// Array.forEach Polyfill
// ======================
// Production steps of ECMA-262, Edition 5, 15.4.4.18
// Reference: http://es5.github.com/#x15.4.4.18
if (!Array.prototype.forEach) {
  Array.prototype.forEach = function (e, t) {
    var n, r;if (this == null) {
      throw new TypeError(" this is null or not defined");
    }var i = Object(this);var s = i.length >>> 0;if (typeof e !== "function") {
      throw new TypeError(e + " is not a function");
    }if (arguments.length > 1) {
      n = t;
    }r = 0;while (r < s) {
      var o;if (r in i) {
        o = i[r];e.call(n, o, r, i);
      }r++;
    }
  };
}
"use strict";

///////////////
// POLYFILLS //
///////////////

//=== trim ===//
if (typeof String.prototype.trim !== "function") {
  String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
  };
}

if (typeof Object.create != "function") {
  (function () {
    var F = function () {};
    Object.create = function (o) {
      if (arguments.length > 1) {
        throw Error("Second argument not supported");
      }
      if (o === null) {
        throw Error("Cannot set a null [[Prototype]]");
      }
      if (typeof o != "object") {
        throw TypeError("Argument must be an object");
      }
      F.prototype = o;
      return new F();
    };
  })();
}


//=== classList ===//
(function () {
  if (typeof window.Element === "undefined" || "classList" in document.documentElement) return;

  var prototype = Array.prototype,
      push = prototype.push,
      splice = prototype.splice,
      join = prototype.join;

  function DOMTokenList(el) {
    this.el = el;
    // The className needs to be trimmed and split on whitespace
    // to retrieve a list of classes.
    var classes = el.className.replace(/^\s+|\s+$/g, "").split(/\s+/);
    for (var i = 0; i < classes.length; i++) {
      push.call(this, classes[i]);
    }
  };

  DOMTokenList.prototype = {
    add: function (token) {
      if (this.contains(token)) return;
      push.call(this, token);
      this.el.className = this.toString();
    },
    contains: function (token) {
      return this.el.className.indexOf(token) != -1;
    },
    item: function (index) {
      return this[index] || null;
    },
    remove: function (token) {
      if (!this.contains(token)) return;
      for (var i = 0; i < this.length; i++) {
        if (this[i] == token) break;
      }
      splice.call(this, i, 1);
      this.el.className = this.toString();
    },
    toString: function () {
      return join.call(this, " ");
    },
    toggle: function (token) {
      if (!this.contains(token)) {
        this.add(token);
      } else {
        this.remove(token);
      }

      return this.contains(token);
    }
  };

  window.DOMTokenList = DOMTokenList;

  function defineElementGetter(obj, prop, getter) {
    if (Object.defineProperty) {
      Object.defineProperty(obj, prop, {
        get: getter
      });
    } else {
      obj.__defineGetter__(prop, getter);
    }
  }

  defineElementGetter(Element.prototype, "classList", function () {
    return new DOMTokenList(this);
  });
})();
//===== end of classlist ====//



//===== array indexOf =====//
if (!("indexOf" in Array.prototype)) {
  Array.prototype.indexOf = function (find, i /*opt*/) {
    if (i === undefined) i = 0;
    if (i < 0) i += this.length;
    if (i < 0) i = 0;
    for (var n = this.length; i < n; i++) if (i in this && this[i] === find) return i;
    return -1;
  };
}





//=== getElementsByClassName ===//
if (!document.getElementsByClassName) {
  document.getElementsByClassName = function (search) {
    var d = document, elements, pattern, i, results = [];
    if (d.querySelectorAll) {
      // IE8
      return d.querySelectorAll("." + search);
    }
    if (d.evaluate) {
      // IE6, IE7
      pattern = ".//*[contains(concat(' ', @class, ' '), ' " + search + " ')]";
      elements = d.evaluate(pattern, d, null, 0, null);
      while (i = elements.iterateNext()) {
        results.push(i);
      }
    } else {
      elements = d.getElementsByTagName("*");
      pattern = new RegExp("(^|\\s)" + search + "(\\s|$)");
      for (i = 0; i < elements.length; i++) {
        if (pattern.test(elements[i].className)) {
          results.push(elements[i]);
        }
      }
    }
    return results;
  };
}
//# sourceMappingURL=app.js.map