var gulp = require('gulp'),
		gutil = require('gulp-util')
		concat = require('gulp-concat'),
		fileInclude = require('gulp-file-include'),
		uglify = require("gulp-uglify"),
		sass = require('gulp-sass'),
		plumber = require('gulp-plumber'),
		livereload = require('gulp-livereload'),
		prefix = require('gulp-autoprefixer'),
    react = require('gulp-react'),
    sourcemaps = require('gulp-sourcemaps'),
    to5 = require('gulp-6to5'),
    webserver = require('gulp-webserver');

var path = {
  html: ['html/pages/*.html'],
  viewHtml: ['html/views/*.html', 'html/elems/*.html'],
	js: ['js/app/util.js', 'js/app/*.js' ],
  jslib: ['js/lib/jquery.min.js', 'js/lib/angular.js', 'js/lib/*.js'],
  jsx: ['js/app/components/*.jsx'],
	sass: ['sass/*.scss']
};

var onError = function(err) {
    gutil.beep();
    gutil.log(err);
};

// Dev watch
// =========
gulp.task('js', function(){
	gulp.src(path.js.concat('./js/app/components/*.js'))
	.pipe(plumber({ errorHandler: onError }))
	.pipe(sourcemaps.init())
	.pipe(to5())
	.pipe(concat('app.js'))
	.pipe(sourcemaps.write('.'))
	.on('error', gutil.log)
	.pipe(gulp.dest('./js'));
});

// handles jsx changes
gulp.task('jsx', ['reactCompile', 'js']);

gulp.task('reactCompile', function(){
  gulp.src(path.jsx)
  .pipe(plumber({ errorHandler: onError }))
  .pipe(react())
  .on('error', gutil.log)
  .pipe(gulp.dest('./js/app/components/'));
});


// because lib doesn't change often, saves needless gulp cycles
gulp.task('jslib', function(){
  gulp.src(path.jslib)
  .pipe(concat('lib.js'))
  .pipe(gulp.dest('js'));
});

gulp.task('sass', function(){
	gulp.src(path.sass)
		.pipe(plumber({ errorHandler: onError }))
		.pipe(sass( {style:'compressed'} ))
		.pipe(prefix())
		.on('error', gutil.log)
		.pipe(gulp.dest('css'));
});

gulp.task('html', function(){
  gulp.src(path.html)
  .pipe(plumber({ errorHandler: onError }))
  .pipe(fileInclude({
  	prefix: '@@',
  	basepath: './html'
  }))
  .on('error', gutil.log)
  .pipe(gulp.dest(''))
});

gulp.task('server', function(){
  gulp.src('')
  .pipe(webserver());
});


// Dist
// =======
gulp.task('js-dist', function(){
  gulp.src(['./js/lib.js', './js/app.js'])
  .pipe( concat('app.js'))
  .pipe( uglify() )
  .pipe(gulp.dest('./dist/js'));
});

gulp.task('html-dist', function(){
  var dest = './dist/';
  for(var str in path.html){
    if(str !== 'root'){ dest += str; }

    gulp.src(path.html[str])
    .pipe(gulp.dest(dest))
  }
});



/***** Watches *****/

gulp.task('watch', function(){
	livereload.listen();
	gulp.watch(['*.html', 'css/app.css', 'js/app.js']).on('change', livereload.changed);
  gulp.watch(path.html, ['html']);
  gulp.watch(path.viewHtml, ['html']);
  gulp.watch(path.js, ['js']);
  gulp.watch(path.jslib, ['jslib']);
	gulp.watch(path.sass, ['sass']);
});


 





gulp.task('dist', ['jslib', 'js', 'js-dist', 'html-dist']);

gulp.task('default', ['server', 'watch' /*, 'watch-react' */]);

