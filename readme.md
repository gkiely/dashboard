Dashboard Project
=================

**Contributors:** Grant Kiely

Build requirements
----
* Node & Gulp js


To Start project:

```sh
cd folder
npm install
npm install gulp
gulp
```

Any gulp packages that are not found, `npm install gulp-package`.

Site is viewable at `localhost:8000`